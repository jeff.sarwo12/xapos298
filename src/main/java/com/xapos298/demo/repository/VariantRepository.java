package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.Variant;

public interface VariantRepository extends JpaRepository<Variant, Long> {
	List<Variant> findByisActive(Boolean active);
	List<Variant> findByisActiveAndCreatedBy(Boolean active,String user);
	
	@Query(value = "SELECT * FROM variant WHERE is_active = ?1 AND created_by = ?2", nativeQuery = true)
	List<Variant> findByVariant(Boolean active,String user);
	
	@Query(value = "SELECT * FROM variant v "
			+ "JOIN category c "
			+ "ON c.id = v.category_id "
			+ "WHERE v.category_id = ?1 "
			+ "AND v.is_active = ?2", nativeQuery = true)
	List<Variant> findByCategory(Long id,Boolean user);
	
	List<Variant> findByisActiveAndCategoryId(Boolean active, Long id);

	@Query(value = "SELECT * FROM variant WHERE lower(variant_name)"
			+ "LIKE lower(concat('%',?1,'%'))",nativeQuery = true)
	List<Variant> findAllByKey(String key);
}


