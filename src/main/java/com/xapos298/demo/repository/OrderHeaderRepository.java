package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.OrderHeader;

public interface OrderHeaderRepository extends JpaRepository<OrderHeader, Long> {

	@Query("SELECT MAX(id) FROM OrderHeader")
	public Long findByMaxId();

	@Query(value = "SELECT * FROM order_header WHERE NOT amount = '0'", nativeQuery = true)
	List<OrderHeader> findByAmnt();
	
	@Query(value="SELECT id FROM order_header WHERE amount = '0' LIMIT 1",nativeQuery = true)
	public Long findIdByAmount();

	
}
