package com.xapos298.demo.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.model.Product;
import com.xapos298.demo.model.Variant;
import com.xapos298.demo.repository.CategoryRepository;
import com.xapos298.demo.repository.ProductRepository;
import com.xapos298.demo.repository.VariantRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiProductController {
	@Autowired
	public ProductRepository productRepository;
	
	@Autowired
	public VariantRepository variantRepository;
	
	@Autowired
	public CategoryRepository categoryRepository;
	
	@GetMapping("product")
	public ResponseEntity<List<Product>> getAllProduct(){
		try {
			List<Product> listProduct = this.productRepository.findByisActive(true);
			return new ResponseEntity<List<Product>>(listProduct, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			// TODO: handle exception
		}
	}
	
	@GetMapping("product/getvar/{id}")
	public ResponseEntity<List<Variant>> getVariantByCategoryId(@PathVariable("id")Long id){
		try {
			List<Variant> listVariant = this.variantRepository.findByisActiveAndCategoryId(true, id);
			return new ResponseEntity<List<Variant>>(listVariant, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			// TODO: handle exception
		}
	}
	
	@PostMapping("product/add")
	public ResponseEntity<Object> saveProduct(@RequestBody Product product){
		product.setCreateBy("user1");
		product.setCreateDate(new Date());
		Product productData = this.productRepository.save(product);
		if (productData.equals(product)) {
			return new ResponseEntity<>("Save Data Successfully",HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Data Successfully",HttpStatus.OK);

		}
	}
	
	@GetMapping("product/{id}")
	public ResponseEntity<List<Product>> getProductById(@PathVariable("id")Long id){
		try {
			Optional<Product> product = this.productRepository.findById(id);
			if (product.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(product,HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
				}
		} catch (Exception e) {
			return new ResponseEntity<List<Product>>(HttpStatus.NO_CONTENT);
		}
	}
	

//	Update Data product
	@PutMapping("edit/product/{id}")
	public ResponseEntity<Object> editVariant(@PathVariable("id") Long id,
			@RequestBody Product product){
		Optional<Product> productData = this.productRepository.findById(id);
		if (productData.isPresent()) {
			product.setId(id);
			product.setModifyBy("user1");
			product.setModifyDate(Date.from(Instant.now()));
			product.setCreateBy(productData.get().getCreateBy());
			product.setCreateDate(productData.get().getCreateDate());
			this.productRepository.save(product);
			
			return new ResponseEntity<Object>("Update Successfully",HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}


//	Hapus
	@PutMapping("delete/product/{id}")
	public ResponseEntity<Object> deleteVariant(@PathVariable("id")Long id){
		Optional<Product> productData = this.productRepository.findById(id);
		if (productData.isPresent()) {
			Product product = new Product();
			product.setId(id);
			product.setIsActive(false);
			product.setModifyBy("user1");
			product.setModifyDate(Date.from(Instant.now()));
			product.setProductName(productData.get().getProductName());
			product.setProductInitial(productData.get().getProductInitial());
			product.setProductDescription(productData.get().getProductDescription());
			product.setProductPrice(productData.get().getProductPrice());
			product.setProductStok(productData.get().getProductStok());
			product.setCreateBy(productData.get().getCreateBy());
			product.setCreateDate(productData.get().getCreateDate());
			product.setVariantId(productData.get().getVariantId());
			this.productRepository.save(product);
			return new ResponseEntity<Object>("Delete Successfully",HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	

//	Fitur Search
	@GetMapping("product/search/{key}")
	public ResponseEntity<List<Product>> getCategoryByKey(@PathVariable("key")String key){
		try {
			List<Product> listProduct = this.productRepository.findAllByKey(key);
			return new ResponseEntity<List<Product>>(listProduct, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}
