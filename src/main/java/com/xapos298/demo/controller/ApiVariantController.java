package com.xapos298.demo.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.model.Variant;
import com.xapos298.demo.repository.CategoryRepository;
import com.xapos298.demo.repository.VariantRepository;



@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiVariantController {
	@Autowired
	public CategoryRepository categoryRepository;
	
	@Autowired
	public VariantRepository variantRepository;

//	Mengambil semua data variant
	@GetMapping("variant")
	public ResponseEntity<List<Variant>> getAllVariant(){
		try {
			List<Variant> listVariant = this.variantRepository.findByisActive(true);
			return new ResponseEntity<List<Variant>>(listVariant, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
//  Menyimpan data variant
	@PostMapping("variant/add")
	public ResponseEntity<Object> saveVariant(@RequestBody Variant variant){
		variant.setCreatedBy("user1");
		variant.setCreatedDate(new Date());
		Variant variantData = this.variantRepository.save(variant);
		if (variantData.equals(variant)) {
			return new ResponseEntity<>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed",HttpStatus.BAD_REQUEST);
		}
	}
	
//	mengamnbil file berdasarkan id
	@GetMapping("variant/{id}")
	public ResponseEntity<List<Variant>> getVariantById(@PathVariable("id") Long id){
		try {
			Optional<Variant> variant = this.variantRepository.findById(id);
			if (variant.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(variant,HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Variant>>(HttpStatus.NO_CONTENT);
		}
	}
	
//	Update Data variant
	@PutMapping("edit/variant/{id}")
	public ResponseEntity<Object> editVariant(@PathVariable("id") Long id,
			@RequestBody Variant variant){
		Optional<Variant> variantData = this.variantRepository.findById(id);
		if (variantData.isPresent()) {
			variant.setId(id);
			variant.setModifyBy("user1");
			variant.setModifyDate(Date.from(Instant.now()));
			variant.setCreatedBy(variantData.get().getCreatedBy());
			variant.setCreatedDate(variantData.get().getCreatedDate());
			this.variantRepository.save(variant);
			
			return new ResponseEntity<Object>("Update Successfully",HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
//	Hapus
	@PutMapping("delete/variant/{id}")
	public ResponseEntity<Object> deleteVariant(@PathVariable("id")Long id){
		Optional<Variant> variantData = this.variantRepository.findById(id);
		if (variantData.isPresent()) {
			Variant variant = new Variant();
			variant.setId(id);
			variant.setIsActive(false);
			variant.setModifyBy("user1");
			variant.setModifyDate(Date.from(Instant.now()));
			variant.setCreatedBy(variantData.get().getCreatedBy());
			variant.setCreatedDate(variantData.get().getCreatedDate());
			variant.setVariantName(variantData.get().getVariantName());
			variant.setVariantInitial(variantData.get().getVariantInitial());
			this.variantRepository.save(variant);
			return new ResponseEntity<Object>("Delete Successfully",HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	

//	Fitur Search
	@GetMapping("variant/search/{key}")
	public ResponseEntity<List<Variant>> getCategoryByKey(@PathVariable("key")String key){
		try {
			List<Variant> listVariant = this.variantRepository.findAllByKey(key);
			return new ResponseEntity<List<Variant>>(listVariant, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}
